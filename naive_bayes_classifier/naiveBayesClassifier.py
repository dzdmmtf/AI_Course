import pandas as pd
import random


def readDataSet():
    df = pd.read_csv('./Data/train_test.csv', encoding='utf-8')
    return df

def pickTrainSet(df, size):
    trainIndexes = []
    while True:
        rand = random.randint(0, size-1)
        if rand not in trainIndexes:
            trainIndexes.append(rand)
        if len(trainIndexes) > size*(8/10) : 
            break
    evaluateSet = set(list(range(size))) - set(trainIndexes)
    return trainIndexes, evaluateSet

def createDictionary(df, trainIndexes):
    wordsDict = dict()
    hafezCnt, saadiCnt = 0, 0
    hafezWords, saadiWords = 0, 0

    for i in trainIndexes:
        hemistichWords = df['text'][i].split(' ')

        if df['label'][i] == 'hafez':
            hafezCnt += 1
            hafezWords += len(hemistichWords)
        else:
            saadiCnt += 1
            saadiWords += len(hemistichWords)

        for word in hemistichWords:
            if word not in wordsDict:
                if df['label'][i] == 'hafez':
                    wordsDict[word] = [1,0]
                else:
                    wordsDict[word] = [0,1]
            else:
                tmp = wordsDict.get(word)
                if df['label'][i] == 'hafez':
                    wordsDict[word] = [tmp[0] + 1, tmp[1]]
                else:
                    wordsDict[word] = [tmp[0], tmp[1] + 1]
    return wordsDict, hafezCnt, saadiCnt, hafezWords, saadiWords

def countPoets(df, evaluateIndexes):
    hcnt, scnt = 0, 0
    for i in evaluateIndexes:
        if df['label'][i] == 'hafez':
            hcnt += 1
        else:
            scnt += 1
    return hcnt, scnt

def calculateProbabilityLaplas(size, wordsDict, hafezWords, saadiWords, hafezCnt, saadiCnt, trainIndexes, evaluateIndexes):
    correctDetectedHafezes = 0
    detectedHafezes = 0
    correctDetected = 0
    alpha = 1/6
    
    for i in evaluateIndexes:
        hafezProb, saadiProb = 1, 1
        words = df['text'][i].split(' ')
        for word in words:
            if word in wordsDict:
                hafezProb *= ((wordsDict.get(word)[0] + alpha) / (hafezWords + (alpha*len(wordsDict))))
                saadiProb *= ((wordsDict.get(word)[1] + alpha) / (saadiWords + (alpha*len(wordsDict))))
                
        hafezProb *= (hafezWords / (hafezWords + saadiWords))
        saadiProb *= (saadiWords / (hafezWords + saadiWords))

        if hafezProb > saadiProb :
            detectedHafezes += 1
            if df['label'][i] == 'hafez' :
                correctDetectedHafezes += 1
                correctDetected += 1
        else :
            if df['label'][i] == 'saadi':
                correctDetected += 1

    allHafez, allSaadi = countPoets(df, evaluateIndexes)
    print('Using Laplas:\n')
    print('Recall is: ', correctDetectedHafezes/allHafez)
    print('Precision is: ', correctDetectedHafezes/detectedHafezes)
    print('Accuracy is: ', correctDetected/len(evaluateIndexes))


def calculateProbability(size, wordsDict, hafezWords, saadiWords, hafezCnt, saadiCnt, trainIndexes, evaluateIndexes):
    correctDetectedHafezes = 0
    detectedHafezes = 0
    correctDetected = 0
    
    for i in evaluateIndexes:
        hafezProb, saadiProb = 1, 1
        words = df['text'][i].split(' ')
        for word in words:
            if word in wordsDict:
                hafezProb *= (wordsDict.get(word)[0] / hafezWords)
                saadiProb *= (wordsDict.get(word)[1] / saadiWords)
                
        hafezProb *= (hafezWords / (hafezWords + saadiWords))
        saadiProb *= (saadiWords / (hafezWords + saadiWords))

        if hafezProb > saadiProb :
            detectedHafezes += 1
            if df['label'][i] == 'hafez' :
                correctDetectedHafezes += 1
                correctDetected += 1
        else :
            if df['label'][i] == 'saadi':
                correctDetected += 1

    allHafez, allSaadi = countPoets(df, evaluateIndexes)
    print('Without Laplas:\n')
    print('Recall is: ', correctDetectedHafezes/allHafez)
    print('Precision is: ', correctDetectedHafezes/detectedHafezes)
    print('Accuracy is: ', correctDetected/len(evaluateIndexes))
    

def evaluationFunction(wordsDict, hafezWords, saadiWords):
    inputDf = pd.read_csv('./Data/evaluate.csv', encoding='utf-8')
    output = pd.DataFrame(columns = ['id', 'label'])
    for i in range(len(inputDf['id'])):
        alpha = 1/6
        hafezProb, saadiProb = 1, 1
        words = inputDf['text'][i].split(' ')
        for word in words:
            if word in wordsDict:
                hafezProb *= ((wordsDict.get(word)[0] + alpha) / (hafezWords + (alpha*len(wordsDict))))
                saadiProb *= ((wordsDict.get(word)[1] + alpha) / (saadiWords + (alpha*len(wordsDict))))
                
        hafezProb *= (hafezWords / (hafezWords + saadiWords))
        saadiProb *= (saadiWords / (hafezWords + saadiWords))

        if hafezProb > saadiProb :
            output = output.append({'id': i+1, 'label': 'hafez'}, ignore_index = True)
        else :
            output = output.append({'id': i+1, 'label': 'saadi'}, ignore_index = True)
    output.to_csv('evaluate.csv', index = False)


if __name__ == "__main__":
    df = readDataSet()
    trainIndexes, evaluateIndexes = pickTrainSet(df, len(df))
    print('Training...')
    wordsDict, hafezCnt, saadiCnt, hafezWords, saadiWords = createDictionary(df, trainIndexes)
    print('Evaluating...')
    calculateProbability(len(df), wordsDict, hafezWords, saadiWords, hafezCnt, saadiCnt, trainIndexes, evaluateIndexes)
    calculateProbabilityLaplas(len(df), wordsDict, hafezWords, saadiWords, hafezCnt, saadiCnt, trainIndexes, evaluateIndexes)
    evaluationFunction(wordsDict, hafezWords, saadiWords)