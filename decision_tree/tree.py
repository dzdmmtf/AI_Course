import pandas as pd
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split
from sklearn import metrics
from sklearn.tree import export_graphviz
from subprocess import call
from IPython.display import Image
from sklearn import model_selection
from sklearn.ensemble import BaggingClassifier
from random import randrange


def readData():
    data = pd.read_csv('data.csv')
    cols = data.columns
    cols = list(data.columns.values.tolist())
    del cols[-1]
    return data, cols


def splitTrainTest():
    X = data[cols]
    Y = data.target
    xTrain, xTest, yTrain, yTest = train_test_split(X, Y, test_size=0.2)

    frames = [xTrain, yTrain]
    train = pd.concat(frames, axis=1)
    return xTrain, xTest, yTrain, yTest, train


def buildDecisionTree(xTrain, xTest, yTrain, yTest, whichPart):
    tree = DecisionTreeClassifier()
    tree = tree.fit(xTrain, yTrain)

    yPred = tree.predict(xTest)
    accuracy = metrics.accuracy_score(yTest, yPred)
    if(whichPart != "part2.5" and whichPart != "part2.2" ):
        print("Accuracy of ", whichPart, " : ", accuracy)
    return tree, yPred


def createImage():
    export_graphviz(tree, 'tree.dot', rounded=True, feature_names=cols,
                    class_names=['yes', 'no'], filled=True)
    call(['dot', '-Tpng', 'tree.dot', '-o', 'tree.png', '-Gdpi=400'])
    Image('tree.png')


def create5Tree():
    batch = []
    for i in range(5):
        batch.append(train.sample(n=150, replace=True))
    return batch


def bagging():
    yPrediction = []
    batch = create5Tree()
    for i in range(5):
        result = buildDecisionTree(batch[i].drop('target', axis=1), xTest, batch[i]['target'], yTest, 'part2.2')
        yPrediction.append(result[1])
  
    outputs = list(map(sum, zip(*yPrediction)))
    res = []
    for i in range(len(outputs)):
        if outputs[i] >= 3 :
            res.append(1)
        else:
            res.append(0)

    accuracy = metrics.accuracy_score(yTest, res)
    print('bagging accuracy: ', accuracy)


def removeOneProp():
    for prop in cols:
        xTrainCopy = xTrain.copy()
        xTrainCopy.drop(prop, axis=1)
        xTestCopy = xTest.copy()
        xTestCopy.drop(prop, axis=1)
        print('Removing ', prop, ' :')
        buildDecisionTree(xTrainCopy, xTestCopy, yTrain, yTest, 'part2.3')


def removeRandomProp():
    randCols = []
    for i in range(8):
        randCols.append(randrange(len(cols)))

    xTrainCopy = xTrain.copy()
    xTestCopy = xTest.copy()
    for a in randCols:
        xTrainCopy.drop(cols[a], axis=1)
        xTestCopy.drop(cols[a], axis=1)

    buildDecisionTree(xTrainCopy, xTestCopy, yTrain, yTest, 'part2.4')


def randomForest():
    randCols = []
    for i in range(8):
        randCols.append(randrange(len(cols)))

    yPrediction = []
    batch = create5Tree()
    for i in range(len(batch)):
        xTrain = batch[i].drop('target', axis=1)
        yTrain = batch[i]['target']

        xTrainCopy = xTrain.copy()
        xTestCopy = xTest.copy()
        for a in randCols:
            xTrainCopy.drop(cols[a], axis=1)
            xTestCopy.drop(cols[a], axis=1)
        result = buildDecisionTree(xTrainCopy, xTestCopy, batch[i]['target'], yTest, 'part2.5')
        yPrediction.append(result[1])

    outputs = list(map(sum, zip(*yPrediction)))
    res = []
    for i in range(len(outputs)):
        if outputs[i] >= 3 :
            res.append(1)
        else:
            res.append(0)

    accuracy = metrics.accuracy_score(yTest, res)
    print('random forest accuracy: ', accuracy)


if __name__ == "__main__":
    data, cols = readData()
    xTrain, xTest, yTrain, yTest, train = splitTrainTest()
    tree, yPred = buildDecisionTree(xTrain, xTest, yTrain, yTest, 'part1.1')
    createImage()
    bagging()
    removeOneProp()
    removeRandomProp()
    randomForest()