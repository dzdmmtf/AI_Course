from __future__ import division
import math
import random
import functools
import numpy as np
import matplotlib.pyplot as plt
from utility import alphabetize, abs_mean


def sigmoid(x):
    return 1/(1+math.exp(-x))


class ValuedElement(object):
    def __init__(self,name,val):
        self.my_name = name
        self.my_value = val

    def set_value(self,val):
        self.my_value = val

    def get_value(self):
        return self.my_value

    def get_name(self):
        return self.my_name

    def __repr__(self):
        return "%s(%1.2f)" %(self.my_name, self.my_value)


class DifferentiableElement(object):
    def output(self):
        raise NotImplementedError("This is an abstract method")

    def dOutdX(self, elem):
        raise NotImplementedError("This is an abstract method")

    def clear_cache(self):
        """clears any precalculated cached value"""
        pass


class Input(ValuedElement,DifferentiableElement):
    def __init__(self,name,val):
        ValuedElement.__init__(self,name,val)
        DifferentiableElement.__init__(self)

    def output(self):
        return self.get_value()

    def dOutdX(self, elem):
        return 0


class Weight(ValuedElement):
    def __init__(self,name,val):
        ValuedElement.__init__(self,name,val)
        self.next_value = None

    def set_next_value(self,val):
        self.next_value = val

    def update(self):
        self.my_value = self.next_value


class Neuron(DifferentiableElement):
    def __init__(self, name, inputs, input_weights, use_cache=True):
        assert len(inputs)==len(input_weights)
        for i in range(len(inputs)):
            assert isinstance(inputs[i],(Neuron,Input))
            assert isinstance(input_weights[i],Weight)
        DifferentiableElement.__init__(self)
        self.my_name = name
        self.my_inputs = inputs # list of Neuron or Input instances
        self.my_weights = input_weights # list of Weight instances
        self.use_cache = use_cache
        self.clear_cache()
        self.my_descendant_weights = None
        self.my_direct_weights = None

    def get_descendant_weights(self):
        if self.my_descendant_weights is None:
            self.my_descendant_weights = {}
            inputs = self.get_inputs()
            weights = self.get_weights()
            for i in range(len(weights)):
                weight = weights[i]
                weight_name = weight.get_name()
                self.my_descendant_weights[weight_name] = set()
                input = inputs[i]
                if not isinstance(input, Input):
                    descendants = input.get_descendant_weights()
                    for name, s in descendants.items():
                        st = self.my_descendant_weights[weight_name]
                        st = st.union(s)
                        st.add(name)
                        self.my_descendant_weights[weight_name] = st

        return self.my_descendant_weights

    def isa_descendant_weight_of(self, target, weight):
        weights = self.get_descendant_weights()
        if weight.get_name() in weights:
            return target.get_name() in weights[weight.get_name()]
        else:
            raise Exception("weight %s is not connect to this node: %s"
                            %(weight, self))

    def has_weight(self, weight):
        return weight.get_name() in self.get_descendant_weights()

    def has_descendant_weight(self, weight):
        if self.has_weight(weight):
            return True
        for i in self.get_inputs():
            if isinstance(i, Neuron):
                if i.has_descendant_weight(weight):
                    return True
        return False

    def get_weight_nodes(self):
        return self.my_weights

    def clear_cache(self):
        self.my_output = None
        self.my_doutdx = {}

    def output(self):
        if self.use_cache:
            # caching optimization, saves previously computed output.
            if self.my_output is None:
                self.my_output = self.compute_output()
            return self.my_output
        return self.compute_output()

    def compute_output(self):
        return sigmoid(sum([w.get_value() * x.output() for w, x in zip(self.get_weights(), self.get_inputs())]))

    def dOutdX(self, elem):
        if self.use_cache:
            # caching optimization, saves previously computed dOutdx.
            if elem not in self.my_doutdx:
                self.my_doutdx[elem] = self.compute_doutdx(elem)
            return self.my_doutdx[elem]
        return self.compute_doutdx(elem)

    def compute_doutdx(self, elem):
        if self.has_descendant_weight(elem):
            return self.output() * (1 - self.output()) * sum(
                [i.output() if w.get_name() == elem.get_name() else w.get_value() * i.dOutdX(elem) for w, i in
                 zip(self.get_weights(), self.get_inputs())])
        else:
            return 0

    def get_weights(self):
        return self.my_weights

    def get_inputs(self):
        return self.my_inputs

    def get_name(self):
        return self.my_name

    def __repr__(self):
        return "Neuron(%s)" %(self.my_name)


class PerformanceElem(DifferentiableElement):
    def __init__(self, input, desired_value):
        assert isinstance(input, (Input, Neuron))
        DifferentiableElement.__init__(self)
        self.my_input = input
        self.my_desired_val = desired_value

    def output(self):
        return -0.5*(self.my_desired_val - self.get_input().output())**2

    def dOutdX(self, elem):
        return (self.my_desired_val - self.get_input().output()) * self.get_input().dOutdX(elem)

    def set_desired(self,new_desired):
        self.my_desired_val = new_desired

    def get_input(self):
        return self.my_input


class Network(object):
    def __init__(self, performance_node, neurons):
        self.inputs = []
        self.weights = []
        self.performance = performance_node
        self.output = performance_node.get_input()
        self.neurons = neurons[:]
        self.neurons.sort(key=functools.cmp_to_key(alphabetize))
        for neuron in self.neurons:
            self.weights.extend(neuron.get_weights())
            for i in neuron.get_inputs():
                if isinstance(i, Input) and not ('i0' in i.get_name()) and not i in self.inputs:
                    self.inputs.append(i)
        self.weights.reverse()
        self.weights = []
        for n in self.neurons:
            self.weights += n.get_weight_nodes()

    @classmethod
    def from_layers(self, performance_node, layers):
        neurons = []
        for layer in layers:
            if layer.get_name() != 'l0':
                neurons.extend(layer.get_elements())
        return Network(performance_node, neurons)

    def clear_cache(self):
        for n in self.neurons:
            n.clear_cache()


def seed_random():
    random.seed(0)
    np.random.seed(0)


def random_weight():
    return random.randrange(-1, 2)


def make_neural_net_basic():
    i0 = Input('i0', -1.0)
    i1 = Input('i1', 0.0)
    i2 = Input('i2', 0.0)

    w1A = Weight('w1A', 1)
    w2A = Weight('w2A', 1)
    wA = Weight('wA', 1)

    A = Neuron('A', [i1, i2, i0], [w1A, w2A, wA])
    P = PerformanceElem(A, 0.0)

    net = Network(P, [A])
    return net


def make_neural_net_two_layer():
    i0 = Input('i0', -1.0)
    i1 = Input('i1', 0.0)
    i2 = Input('i2', 0.0)

    seed_random()
    w1A = Weight('w1A', random_weight())
    w1B = Weight('w1B', random_weight())
    w2A = Weight('w2A', random_weight())
    w2B = Weight('w2B', random_weight())
    wA = Weight('wA', random_weight())
    wB = Weight('wB', random_weight())
    wC = Weight('wC', random_weight())
    wAC = Weight('wAC', random_weight())
    wBC = Weight('wBC', random_weight())

    A = Neuron('A', [i0, i1, i2], [wA, w1A, w2A])
    B = Neuron('B', [i0, i1, i2], [wB, w1B, w2B])
    C = Neuron('C', [i0, A, B], [wC, wAC, wBC])
    P = PerformanceElem(C, 0.0)
    net = Network(P, [A, B, C])
    return net


def make_neural_net_challenging():
    i0 = Input('i0', -1.0)
    i1 = Input('i1', 0.0)
    i2 = Input('i2', 0.0)

    seed_random()
    w1A = Weight('w1A', random_weight())
    w1B = Weight('w1B', random_weight())
    w1C = Weight('w1C', random_weight())
    w2A = Weight('w2A', random_weight())
    w2B = Weight('w2B', random_weight())
    w2C = Weight('w2C', random_weight())
    wA = Weight('wA', random_weight())
    wB = Weight('wB', random_weight())
    wC = Weight('wC', random_weight())
    wD = Weight('wD', random_weight())
    wAD = Weight('wAD', random_weight())
    wBD = Weight('wBD', random_weight())
    wCD = Weight('wCD', random_weight())

    A = Neuron('A', [i0, i1, i2], [wA, w1A, w2A])
    B = Neuron('B', [i0, i1, i2], [wB, w1B, w2B])
    C = Neuron('C', [i0, i1, i2], [wC, w1C, w2C])
    D = Neuron('D', [i0, A, B, C], [wD, wAD, wBD, wCD])
    P = PerformanceElem(D, 0.0)
    net = Network(P, [A, B, C, D])
    return net


def make_neural_net_two_moons():
    i0 = Input('i0', -1.0)
    i1 = Input('i1', 0.0)
    i2 = Input('i2', 0.0)

    seed_random()
    second_layer_neurons = 40
    pre = '0'
    hidden_neurons, output_weights = [], []

    bias_weight = Weight('wB', random_weight())
    output_weights.append(bias_weight)

    for i in range(second_layer_neurons):
        if i >= 10:
            pre = '1'
        w1 = Weight('w1A' + pre + str(i+1), random_weight())
        w2 = Weight('w2A' + pre + str(i+1), random_weight())
        w = Weight('wA' + pre + str(i+1), random_weight())
        hidden_neurons.append(Neuron('A' + pre + str(i+1), [i0, i1, i2], [w, w1, w2]))

        w_out = Weight('wA' + pre + str(i+1) + 'B', random_weight())
        output_weights.append(w_out)

    output_neuron = Neuron('B', [i0] + hidden_neurons, output_weights)

    # p = PerformanceElem(output_neuron, 0.0)
    p = RegularizedPerformanceElem(output_neuron, 0.0)
    net = Network(p, hidden_neurons + [output_neuron])
    return net


def sum_square_weights(network_weights):
    sigma = 0
    for w in network_weights:
        sigma += w.get_value() ** 2
    return sigma


class RegularizedPerformanceElem(PerformanceElem):

    def __init__(self, input, desired_value):
        assert isinstance(input, (Input, Neuron))
        DifferentiableElement.__init__(self)
        self.my_input = input
        self.my_desired_val = desired_value
        self.my_lambda = 10 ** (-3)

    def get_network_weights(self, neuron, weights):
        if type(neuron) == Input:
            return weights
        else:
            weights += neuron.get_weights()
            for my_input in neuron.get_inputs():
                weights = self.get_network_weights(my_input, weights)
            return weights

    def output(self):
        network_weights = self.get_network_weights(self.my_input, [])
        sigma = self.my_lambda * sum_square_weights(network_weights)
        return (-0.5) * ((self.my_desired_val - self.my_input.output()) ** 2) - sigma

    def dOutdX(self, elem):
        phrase = elem.get_value() * self.my_lambda * 2
        return ((self.my_desired_val - self.my_input.output()) * self.my_input.dOutdX(elem)) - phrase


def finite_difference(network):
    epsilon = 10 ** (-8)
    for w in network.weights:
        old_weight = w.get_value()
        network.clear_cache()
        function_derivation = network.performance.dOutdX(w)
        output = network.performance.output()
        w.set_value(old_weight + epsilon)
        network.clear_cache()
        epsilon_output = network.performance.output()
        w.set_value(old_weight)
        finite_diff = (epsilon_output - output) / epsilon
        if abs(finite_diff - function_derivation) > 0.005:
            return False
    return True


def plot_decision_boundary(network, xmin, xmax, ymin, ymax):
    n = 100
    step = float((xmax - xmin) / n)
    x_points, y_points, under_half_x, under_half_y = [], [], [], []
    for i in range(n):
        x_points.append(xmin + (i * step))
        y_points.append(ymin + (i * step))

    for x in x_points:
        for y in y_points:
            network.inputs[0].set_value(x)
            network.inputs[1].set_value(y)
            network.clear_cache()
            output = network.output.output()
            network.clear_cache()
            if output < 0.5:
                under_half_x.append(x)
                under_half_y.append(y)

    plt.scatter(under_half_x, under_half_y, color='b')
    plt.title('Outputs Less than 0.5')
    plt.xlabel('x')
    plt.ylabel('y')
    plt.show()


def train(network, data, rate=1.0, target_abs_mean_performance=0.0001, max_iterations=10000, verbose=False):
    iteration = 0
    while iteration < max_iterations:
        performances = []
        for datum in data:
            for i in range(len(network.inputs)):
                network.inputs[i].set_value(datum[i])

            network.performance.set_desired(datum[-1])

            network.clear_cache()

            for w in network.weights:
                w.set_next_value(w.get_value() +
                                 rate * network.performance.dOutdX(w))

            for w in network.weights:
                w.update()

            performances.append(network.performance.output())

            network.clear_cache()

        abs_mean_performance = abs_mean(performances)

        if abs_mean_performance < target_abs_mean_performance:
            if verbose:
                print("iter %d: training complete.\n"\
                      "mean-abs-performance threshold %s reached (%1.6f)"\
                      %(iteration,
                        target_abs_mean_performance,
                        abs_mean_performance))
            break

        iteration += 1

        # if iteration % 10 == 0 and verbose:
        #     print("iter %d: mean-abs-performance = %1.6f"\
        #           %(iteration,
        #             abs_mean_performance))
    # print('weights:', network.weights)

    print('finite difference: ', finite_difference(network))
    plot_decision_boundary(network, -4, 4, -4, 4)


def test(network, data, verbose=False):
    correct = 0
    for datum in data:

        for i in range(len(network.inputs)):
            network.inputs[i].set_value(datum[i])

        network.clear_cache()

        result = network.output.output()
        prediction = round(result)

        network.clear_cache()

        if prediction == datum[-1]:
            correct += 1
            # if verbose:
            #     print("test(%s) returned: %s => %s [%s]" % (str(datum), str(result), datum[-1], "correct"))
        # else:
            # if verbose:
            #     print("test(%s) returned: %s => %s [%s]" % (str(datum), str(result), datum[-1], "wrong"))

    return float(correct)/len(data)




